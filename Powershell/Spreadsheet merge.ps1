$path="C:\Users\jscha\Desktop\Spreadsheets"

Get-ChildItem -Path $Path *.csv | foreach { Remove-Item -Path $_.FullName }


foreach($file in (Get-ChildItem $path)) {

  $newname = $file.FullName -replace '\.xls$', '.csv'
  $ExcelWB = new-object -comobject excel.application
  $Workbook = $ExcelWB.Workbooks.Open($file.FullName) 
  $Workbook.SaveAs($newname,6) 
  $Workbook.Close($false)
  $ExcelWB.quit()
}

$getFirstLine = $true

get-childItem $path | foreach {
    $filePath = $_

    $lines = Get-Content $filePath  
    $linesToWrite = switch($getFirstLine) {
           $true  {$lines}
           $false {$lines | Select -Skip 1}

    }

    $getFirstLine = $false
    Add-Content "c:\path\to\files\final.csv" $linesToWrite
    }