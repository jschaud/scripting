import os
import pandas as pd
import glob
import os

#Set Path, variable names, get list of files in folder
path = "C:/Users/jscha/Desktop/Spreadsheets"
mergedFileName="C:/Users/jscha/Desktop/Spreadsheets/Merged.xlsx"

#delete merged file if it exists
if os.path.exists(mergedFileName):
  os.remove(mergedFileName)
  print("File deleted")
else:
  print("The file does not exist")

#Get all files with .xls in path
xlsFileNameList = glob.glob(path+"/*.xls")
xlsDataFrames = []

#put names of files in 
for x in xlsFileNameList:
    df = pd.read_excel(x)
    df = df.drop(["Bid Date", "Design Team City", "Design Team State/Province", "Design Team Contact Name", "Design Team Email", "Design Team Phone Number"], axis=1)
    xlsDataFrames.append(df)
     
appended_df = pd.concat(xlsDataFrames)
appended_df.to_excel(mergedFileName, index=False)